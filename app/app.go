package hello

import (
	"fmt"
	"io"
	"net/http"
)

const (
	API_ACCOUNT = "/api/d3/account/Straton/1"
	API_HERO    = "/api/d3/hero/182"
	HOST        = "d3-api.appspot.com"
)

func init() {
	http.HandleFunc("/", root)
	http.HandleFunc(API_ACCOUNT, account)
	http.HandleFunc(API_HERO, hero)
}

func root(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<a href=%s>%s%s</a><br>", API_ACCOUNT, HOST, API_ACCOUNT)
	fmt.Fprintf(w, "<a href=%s>%s%s</a><br>", API_HERO, HOST, API_HERO)
}

func account(w http.ResponseWriter, r *http.Request) {
	typeJson(w)
	io.WriteString(w, aout)
}

func hero(w http.ResponseWriter, r *http.Request) {
	typeJson(w)
	io.WriteString(w, hout)
}

func typeJson(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json")
}

var aout = `{
    "account": "Straton#1",
    "heroes": [182, 183, 184, 185, 186],
    "artisan": [{
        "slug": "Blacksmith",
        "level": 3,
        "step": { "current": 4, "max": 5 }
    },{
        "slug": "Jeweler",
        "level": 1,
        "step": { "current": 0, "max": 1 }
    }],
    "progression": [{
        "act": 1,
        "difficulty": 0
    },{
        "act": 2,
        "difficulty": -1
    },{
        "act": 3,
        "difficulty": -1
    },{
        "act": 4,
        "difficulty": -1
    }],
    "kills": {
        "monsters": 4280,
        "elites": 241,
         "hardcore_monsters": 0
    },
    "time-played": {
        "barbarian": 1.0,
        "demon-hunter": 0.6,
        "monk": 0.3,
        "witch-doctor": 0.1,
        "wizard": 0.0
    },
    "last_modified ": 1666805944000,
}`

var hout = `{
    "id": 182,
    "name": "Yharr",
    "hardcore": false,
    "hero_class": 1,
    "level": 21,
    "gender": 0,
    "create_time": 1351949944,
    "update_time": 1666805944,
    "hireling": [{
        "hireling_class": 1,
        "level": 18
    }, {
        "hireling_class": 2,
        "level": 20,
        "active": true
    }],
    "skills": {
        "active": [{
            "slug": "barbarian-overpower",
            "name": "Overpower",
            "icon": "http://us.media.blizzard.com/d3/icons/skills/64/barbarian_overpower.png",
            "rune": { "type": 5, "name": "Revel" }
        }],
        "passive": [{
            "slug": "barbarian-brawler",
            "name": "Brawler",
            "icon": " http://us.media.blizzard.com/d3/icons/skills/64/barbarian_passive_brawler.png"
        }]
    },
    "elites_killed": 241,
    "attributes": {
        "life": 1510,
        "dps": 58.8255,
        "armor": 345,    
        "strength": 129,
        "dexterity": 43,
        "intelligence": 50,
        "vitality": 139,
        "resist_arcane": 0,
        "resist_fire": 0,
        "resist_lightning": 0,
        "resist_poison": 0,
        "resist_cold": 0,
        "crit_chance": 0.08,
        "damage_reduction": 0.333756,
        "magic-find": 15,
        "gold-find": 14
    }
}`
